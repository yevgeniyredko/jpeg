Homework. JPEG
==============

## Изменения:
* Поддерживается только Format24bppRgb
* 2D DCT сводится к 1D DCT
* 1D DCT/IDCT заинлайнены в 2D DCT/IDCT
* В качестве алгоритма 1D DCT реализован [https://web.stanford.edu/class/ee398a/handouts/lectures/07-TransformCoding.pdf#page=30](https://web.stanford.edu/class/ee398a/handouts/lectures/07-TransformCoding.pdf#page=30)
* Вместо GetPixel/SetPixel используются указатели
* Вместо Linq для построения дерева используется самописная PriorityQueue
* При разжатии Dictionary преобразуется в двумерный массив
* GetQuantizationMatrix не вызывается при каждом Quantize/DeQuantize
* В CalcFrequences Parallel ForEach заменен на обычный foreach
* Pixel теперь struct
* Pixel содержит 3 поля вместо 6
* После сжатия вызывается GC.Collect() :)
* Распаралеллены преобразования Matrix -> Bitmap и Bitmap -> Matrix

### Примерное сравнение с изначальной версией (i7-4720HQ)
![cmpi7-4720hq](/pic/cmpi7-4720hq.png)