﻿using System;
using System.Globalization;

namespace JPEG
{
	public static class Dct
	{
		public const int DctSize = 8;
		
		// https://web.stanford.edu/class/ee398a/handouts/lectures/07-TransformCoding.pdf#page=30
		public static double[,] Dct2D(double[,] input)
		{
			var result = new double[DctSize, DctSize];
			Array.Copy(input, result, input.Length);
			
			for (int j = 0; j < DctSize; j++)
			{
				var x01 = result[0, j] + result[7, j];
				var x11 = result[1, j] + result[6, j];
				var x21 = result[2, j] + result[5, j];
				var x31 = result[3, j] + result[4, j];
				var x41 = result[3, j] - result[4, j];
				var x51 = result[2, j] - result[5, j];
				var x61 = result[1, j] - result[6, j];
				var x71 = result[0, j] - result[7, j];

				var x02 = x01 + x31;
				var x12 = x11 + x21;
				var x22 = x11 - x21;
				var x32 = x01 - x31;
				var x42 = -x41 - x51;
				var x52 = x51 + x61;
				var x62 = x61 + x71;

				var x03 = x02 + x12;
				var x13 = x02 - x12;
				var x23 = x22 + x32;

				var xn = (x42 + x62) * A[5];

				var x24 = x23 * A[1];
				var x44 = -x42 * A[2] - xn;
				var x54 = x52 * A[3];
				var x64 = x62 * A[4] - xn;

				var x25 = x24 + x32;
				var x35 = x32 - x24;
				var x55 = x54 + x71;
				var x75 = x71 - x54;

				var x46 = x44 + x75;
				var x56 = x55 + x64;
				var x66 = x55 - x64;
				var x76 = x75 - x44;

				result[0, j] = x03 * S[0];
				result[4, j] = x13 * S[4];
				result[2, j] = x25 * S[2];
				result[6, j] = x35 * S[6];
				result[5, j] = x46 * S[5];
				result[1, j] = x56 * S[1];
				result[7, j] = x66 * S[7];
				result[3, j] = x76 * S[3];
			}
			
			for (int j = 0; j < DctSize; j++)
			{
				var x01 = result[j, 0] + result[j, 7];
				var x11 = result[j, 1] + result[j, 6];
				var x21 = result[j, 2] + result[j, 5];
				var x31 = result[j, 3] + result[j, 4];
				var x41 = result[j, 3] - result[j, 4];
				var x51 = result[j, 2] - result[j, 5];
				var x61 = result[j, 1] - result[j, 6];
				var x71 = result[j, 0] - result[j, 7];

				var x02 = x01 + x31;
				var x12 = x11 + x21;
				var x22 = x11 - x21;
				var x32 = x01 - x31;
				var x42 = -x41 - x51;
				var x52 = x51 + x61;
				var x62 = x61 + x71;

				var x03 = x02 + x12;
				var x13 = x02 - x12;
				var x23 = x22 + x32;

				var xn = (x42 + x62) * A[5];

				var x24 = x23 * A[1];
				var x44 = -x42 * A[2] - xn;
				var x54 = x52 * A[3];
				var x64 = x62 * A[4] - xn;

				var x25 = x24 + x32;
				var x35 = x32 - x24;
				var x55 = x54 + x71;
				var x75 = x71 - x54;

				var x46 = x44 + x75;
				var x56 = x55 + x64;
				var x66 = x55 - x64;
				var x76 = x75 - x44;

				result[j, 0] = x03 * S[0];
				result[j, 4] = x13 * S[4];
				result[j, 2] = x25 * S[2];
				result[j, 6] = x35 * S[6];
				result[j, 5] = x46 * S[5];
				result[j, 1] = x56 * S[1];
				result[j, 7] = x66 * S[7];
				result[j, 3] = x76 * S[3];
			}
			
			return result;
		}

		public static void IDct2D(double[,] coeffs, double[,] output)
		{
			Array.Copy(coeffs, output, coeffs.Length);
			
			for (int j = 0; j < DctSize; j++)
			{
				var x03 = output[j, 0] / S[0];
				var x13 = output[j, 4] / S[4];
				var x25 = output[j, 2] / S[2];
				var x35 = output[j, 6] / S[6];
				var x46 = output[j, 5] / S[5];
				var x56 = output[j, 1] / S[1];
				var x66 = output[j, 7] / S[7];
				var x76 = output[j, 3] / S[3];

				var x55 = (x56 + x66) / 2;
				var x64 = x56 - x55;
				var x75 = (x76 + x46) / 2;
				var x44 = x75 - x76;

				var x71 = (x55 + x75) / 2;
				var x54 = x71 - x75;
				var x32 = (x25 + x35) / 2;
				var x24 = x32 - x35;

				var x23 = x24 / A[1];
				var x52 = x54 / A[3];

				var x02 = (x03 + x13) / 2;
				var x12 = x02 - x13;
				var x22 = x23 - x32;

				var x11 = (x12 + x22) / 2;
				var x21 = x11 - x22;
				var x01 = (x02 + x32) / 2;
				var x31 = x01 - x32;
			
				var x62 = (x64 * (A[2] + A[5]) - x44 * A[5]) / ((A[4] - A[5]) * (A[2] + A[5]) + A[5] * A[5]);
				var x42 = (-x44 - x62 * A[5]) / (A[2] + A[5]);

				var x61 = x62 - x71;
				var x51 = x52 - x61;
				var x41 = -x42 - x51;

				output[j, 0] = (x01 + x71) / 2;
				output[j, 1] = (x11 + x61) / 2;
				output[j, 2] = (x21 + x51) / 2;
				output[j, 3] = (x31 + x41) / 2;
				output[j, 4] = output[j, 3] - x41;
				output[j, 5] = output[j, 2] - x51;
				output[j, 6] = output[j, 1] - x61;
				output[j, 7] = output[j, 0] - x71;
			}
			
			for (int j = 0; j < DctSize; j++)
			{
				var x03 = output[0, j] / S[0];
				var x13 = output[4, j] / S[4];
				var x25 = output[2, j] / S[2];
				var x35 = output[6, j] / S[6];
				var x46 = output[5, j] / S[5];
				var x56 = output[1, j] / S[1];
				var x66 = output[7, j] / S[7];
				var x76 = output[3, j] / S[3];

				var x55 = (x56 + x66) / 2;
				var x64 = x56 - x55;
				var x75 = (x76 + x46) / 2;
				var x44 = x75 - x76;

				var x71 = (x55 + x75) / 2;
				var x54 = x71 - x75;
				var x32 = (x25 + x35) / 2;
				var x24 = x32 - x35;

				var x23 = x24 / A[1];
				var x52 = x54 / A[3];

				var x02 = (x03 + x13) / 2;
				var x12 = x02 - x13;
				var x22 = x23 - x32;

				var x11 = (x12 + x22) / 2;
				var x21 = x11 - x22;
				var x01 = (x02 + x32) / 2;
				var x31 = x01 - x32;
			
				var x62 = (x64 * (A[2] + A[5]) - x44 * A[5]) / ((A[4] - A[5]) * (A[2] + A[5]) + A[5] * A[5]);
				var x42 = (-x44 - x62 * A[5]) / (A[2] + A[5]);

				var x61 = x62 - x71;
				var x51 = x52 - x61;
				var x41 = -x42 - x51;

				output[0, j] = (x01 + x71) / 2;
				output[1, j] = (x11 + x61) / 2;
				output[2, j] = (x21 + x51) / 2;
				output[3, j] = (x31 + x41) / 2;
				output[4, j] = output[3, j] - x41;
				output[5, j] = output[2, j] - x51;
				output[6, j] = output[1, j] - x61;
				output[7, j] = output[0, j] - x71;
			}
		}
		
		private static readonly double[] S = new double[8];
		private static readonly double[] A = new double[6];

		static Dct()
		{
			var c = new double[8];

			S[0] = 1 / (2 * Math.Sqrt(2));
			for (int i = 1; i < 8; i++)
			{
				c[i] = Math.Cos(Math.PI * i / 16);
				S[i] = 1 / (4 * c[i]);
			}

			A[1] = c[4];
			A[2] = c[2] - c[6];
			A[3] = c[4];
			A[4] = c[6] + c[2];
			A[5] = c[6];
		}
	}
}