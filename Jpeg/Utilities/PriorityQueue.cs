﻿using System.Collections.Generic;
using System.Linq;

namespace JPEG.Utilities
{
    public class PriorityQueue<T>
    {
        public int Count { get; private set; }

        public void Enqueue(T item, int priority)
        {
            Count++;
            
            if (!sortedDictionary.ContainsKey(priority)) 
                sortedDictionary.Add(priority, new Queue<T>());
            sortedDictionary[priority].Enqueue(item);
        }

        public T Dequeue()
        {
            Count--;
            
            var kvp = sortedDictionary.First();
            if (kvp.Value.Count == 1)
                sortedDictionary.Remove(kvp.Key);

            return kvp.Value.Dequeue();
        }
        
        private readonly SortedDictionary<int, Queue<T>> sortedDictionary = new SortedDictionary<int, Queue<T>>();
    }
}