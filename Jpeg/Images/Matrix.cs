﻿using System.Drawing;
using System.Drawing.Imaging;
using System.Threading.Tasks;

namespace JPEG.Images
{
    class Matrix
    {
        public readonly Pixel[,] Pixels;
        public readonly int Height;
        public readonly int Width;
				
        public Matrix(int height, int width)
        {
            Height = height;
            Width = width;
			
            Pixels = new Pixel[height,width];
        }

        public static explicit operator Matrix(Bitmap bmp)
        {
            var height = bmp.Height - bmp.Height % 8;
            var width = bmp.Width - bmp.Width % 8;
            var matrix = new Matrix(height, width);
            
            var bitmapData = bmp.LockBits(
                new Rectangle(0, 0, width, height), 
                ImageLockMode.ReadOnly, 
                System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            
            unsafe
            {
                var rowPtr = (byte*) bitmapData.Scan0;
                Parallel.For(0, height, j =>
                {
                    var columnPtr = rowPtr + j * bitmapData.Stride;

                    for (var i = 0; i < width; i++)
                    {
                        var b = *(columnPtr++);
                        var g = *(columnPtr++);
                        var r = *(columnPtr++);
                        matrix.Pixels[j, i] = new Pixel(r, g, b, PixelFormat.Rgb);
                    }
                });
            }
            bmp.UnlockBits(bitmapData);

            return matrix;
        }

        public static explicit operator Bitmap(Matrix matrix)
        {
            var width = matrix.Width;
            var height = matrix.Height;
            
            var bmp = new Bitmap(width, height);
            
            var bitmapData = bmp.LockBits(
                new Rectangle(0, 0, width, height), 
                ImageLockMode.ReadWrite, 
                System.Drawing.Imaging.PixelFormat.Format24bppRgb);

            unsafe
            {
                var rowPtr = (byte*) bitmapData.Scan0;
                Parallel.For(0, height, j =>
                {
                    var columnPtr = rowPtr + j * bitmapData.Stride;

                    for (var i = 0; i < width; i++)
                    {
                        var pixel = matrix.Pixels[j, i];
                        *(columnPtr++) = ToByte(pixel.B);
                        *(columnPtr++) = ToByte(pixel.G);
                        *(columnPtr++) = ToByte(pixel.R);
                    }
                });
            }
            bmp.UnlockBits(bitmapData);

            return bmp;
        }

        public static byte ToByte(double d)
        {
            var val = (int) d;
            if (val > byte.MaxValue)
                return byte.MaxValue;
            if (val < byte.MinValue)
                return byte.MinValue;
            return (byte) val;
        }
    }
}