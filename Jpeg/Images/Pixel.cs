﻿using System;
using System.Linq;
using Microsoft.SqlServer.Server;

namespace JPEG.Images
{
    public struct Pixel
    {
        public Pixel(double firstComponent, double secondComponent, double thirdComponent, PixelFormat pixelFormat)
        {
            switch (pixelFormat)
            {
                case PixelFormat.Rgb:
                case PixelFormat.YCbCr:
                    this.format = pixelFormat;
                    break;
                default:
                    throw new FormatException("Unknown pixel format: " + pixelFormat);
            }
            this.firstComponent = firstComponent;
            this.secondComponent = secondComponent;
            this.thirdComponent = thirdComponent;
        }

        private readonly PixelFormat format;
        
        private readonly double firstComponent;
        private readonly double secondComponent;
        private readonly double thirdComponent;
        
        public double R => format == PixelFormat.Rgb ? firstComponent : (298.082 * Y + 408.583 * Cr) / 256.0 - 222.921;
        public double G  => format == PixelFormat.Rgb ? secondComponent : (298.082 * Y - 100.291 * Cb - 208.120 * Cr) / 256.0 + 135.576;
        public double B => format == PixelFormat.Rgb ? thirdComponent : (298.082 * Y + 516.412 * Cb) / 256.0 - 276.836;

        public double Y => format == PixelFormat.YCbCr ? firstComponent : 16.0 + (65.738 * R + 129.057 * G + 24.064 * B) / 256.0;
        public double Cb => format == PixelFormat.YCbCr ? secondComponent : 128.0 + (-37.945 * R - 74.494 * G + 112.439 * B) / 256.0;
        public double Cr => format == PixelFormat.YCbCr ? thirdComponent : 128.0 + (112.439 * R - 94.154 * G - 18.285 * B) / 256.0;
    }
}